# Transity Tile Server

## Instructions
Make sure you have docker installed on your host system.

### Build
`docker build -t <tag_name> --build-arg location=<location> .`

Currently the options available for "location" are:
* us-midwest

### Deploy
`docker run -d -p <PORT>:8080 <tag_name>`