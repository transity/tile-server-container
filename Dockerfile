FROM alpine:latest

ARG location
ENV location ${location}

RUN apk update \
    && apk upgrade --no-cache \
    && apk add --no-cache \
    alpine-sdk \
    openssl-dev \
    git \
    git-lfs \
    nodejs \
    npm

RUN npm -g config set user root

RUN npm install -g tileserver-gl

RUN git lfs install

RUN git clone https://gitlab.com/transity/tile-server.git


WORKDIR /tile-server

COPY . .

EXPOSE 8080

CMD tileserver-gl "${location}.mbtiles"
